# ✨ ***Zachare Lofton*** ✨

## **Software Developer**: *Learning the new things every day*
I am a Computer Science Major at Morehouse College, studying to become a software engineer. I am currently new to **GitLab** and learing the ins and outs of this software. Hopefully I will be able to add this tool to the list of tools I will need to be successful in the field. 

## **Hobbies and Interest**:
### **Hobbies** 🎞🏀🎮
- Video Games🎮🕹
- Movies👽💀👺🦸‍♂️
- Cooking🍳🥩🥡🍮🥂
- Drawing🖼📖✏🖋🖌

### **Interest** 🤓
- Art
- Graphic Design
- Artificial Intellegence
- Basketball
- Anime
## **Experience**:
- Engineering Practicum Intern *Google Inc*, 2016
- Hack-A-Thons
    1. ***Google Hack-A-Thon***, *2015*
    2. ***Google Games***, *2016*
- **[Projects](https://github.com/macka221 "My spectacular github, (still adding stuff)")**

## **Skills:**
### Software Languages💻:
- Python
- C++
### Spoken Languages🗣:
1. English
2. Basic Spanish
### Clubs: 
- Morehouse College Pre-Alumni Association
### Organizations: 
- α λ Δ (Alpha Lambda Delta) Honors Society
- CodeHouse Community



